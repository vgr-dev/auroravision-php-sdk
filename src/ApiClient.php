<?php
namespace vgr\AuroraVision;

use vgr\AuroraVision\Exceptions\AuroraVisionException;
use vgr\AuroraVision\Exceptions\ApiException;

/**
 * Class that wraps the AuroraVision API.
 *
 * @author Victor Gonzalez <victor@vgr.cl>
 */
class ApiClient
{
    const USER_AGENT = 'AuroraVision PHP SDK 1.0';

    const EVENT_STATE_ALL = 'ALL';
    const EVENT_STATE_ACTIVE = 'ACTIVE';
    const EVENT_STATE_CLOSED = 'CLOSED';
    const EVENT_OCCURRENCE_DAY = 'H24';
    const EVENT_OCCURRENCE_WEEK = 'D7';
    const EVENT_OCCURRENCE_MONTH = 'D30';
    const EVENT_KIND_PROFILE = 'PROFILE';
    const EVENT_KIND_SOURCE = 'SOURCE';

    /**
     * Returns all the possible event states.
     *
     * @return array
     */
    public static function eventStates(): array
    {
        return [
            self::EVENT_STATE_ALL,
            self::EVENT_STATE_ACTIVE,
            self::EVENT_STATE_CLOSED,
        ];
    }

    /**
     * Returns all the possible event ocurrences.
     *
     * @return array
     */
    public static function eventOccurrences(): array
    {
        return [
            self::EVENT_OCCURRENCE_DAY,
            self::EVENT_OCCURRENCE_WEEK,
            self::EVENT_OCCURRENCE_MONTH,
        ];
    }

    /**
     * Returns all the possible event kinds.
     *
     * @return array
     */
    public static function eventKinds(): array
    {
        return [
            self::EVENT_KIND_PROFILE,
            self::EVENT_KIND_SOURCE,
        ];
    }

    /**
     * The AuroraVision API key.
     *
     * @var string
     */
    protected $apiKey;

    /**
     * The AuroraVision platform username.
     *
     * @var string
     */
    protected $username;

    /**
     * The AuroraVision platform password.
     * @var string
     */
    protected $password;

    /**
     * The AuroraVision API URL.
     * @var string
     */
    public $url = 'https://api.auroravision.net';

    /**
     * Maximum amount of time in seconds that is allowed to make the connection to the API server
     * @var int
     */
    public $curlConnectTimeout = 20;

    /**
     * Maximum amount of time in seconds to which the execution of cURL call will be limited
     * @var int
     */
    public $curlTimeout = 20;

    /**
     * The header token needed for API requests. Generated at authentication step.
     * @var string
     */
    public $token;

    /**
     * The class constructor.
     *
     * @param string $apiKey The AuroraVision API key.
     * @param string $username The AuroraVision platform username.
     * @param string $password The AuroraVision platform password.
     * @return void
     */
    public function __construct(string $apiKey, string $username, string $password)
    {
        $this->apiKey = $apiKey;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Perform a GET request to the API
     *
     * @param string $path Request path (e.g. 'orders' or 'orders/123')
     * @param array $params Additional GET parameters as an associative array
     * @return mixed API response
     * @throws \AuroraVision\Exceptions\ApiException if the API call status code is not in the 2xx range
     * @throws \AuroraVision\Exceptions\AuroraVisionException if the API call has failed or the response is invalid
     */
    public function get($path, $params = [])
    {
        return $this->request('GET', $path, $params);
    }

    /**
     * Perform a DELETE request to the API
     *
     * @param string $path Request path (e.g. 'orders' or 'orders/123')
     * @param array $params Additional GET parameters as an associative array
     * @return mixed API response
     * @throws \AuroraVision\Exceptions\ApiException if the API call status code is not in the 2xx range
     * @throws \AuroraVision\Exceptions\AuroraVisionException if the API call has failed or the response is invalid
     */
    public function delete($path, $params = [])
    {
        return $this->request('DELETE', $path, $params);
    }

    /**
     * Perform a POST request to the API
     *
     * @param string $path Request path (e.g. 'orders' or 'orders/123')
     * @param array $data Request body data as an associative array
     * @param array $params Additional GET parameters as an associative array
     * @return mixed API response
     * @throws \AuroraVision\Exceptions\ApiException if the API call status code is not in the 2xx range
     * @throws \AuroraVision\Exceptions\AuroraVisionException if the API call has failed or the response is invalid
     */
    public function post($path, $data = [], $params = [])
    {
        return $this->request('POST', $path, $params, $data);
    }

    /**
     * Perform a PUT request to the API
     *
     * @param string $path Request path (e.g. 'orders' or 'orders/123')
     * @param array $data Request body data as an associative array
     * @param array $params Additional GET parameters as an associative array
     * @return mixed API response
     * @throws \AuroraVision\Exceptions\ApiException if the API call status code is not in the 2xx range
     * @throws \AuroraVision\Exceptions\AuroraVisionException if the API call has failed or the response is invalid
     */
    public function put($path, $data = [], $params = [])
    {
        return $this->request('PUT', $path, $params, $data);
    }

    /**
     * Return raw response data from the last request
     *
     * @return string|null Response data
     */
    public function getLastResponseRaw()
    {
        return $this->lastResponseRaw;
    }

    /**
     * Return decoded response data from the last request
     *
     * @return array|null Response data
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * Internal request implementation
     *
     * @param string $method POST, GET, etc.
     * @param string $path
     * @param array $params
     * @param mixed $data
     * @return void
     * @throws \AuroraVision\Exceptions\ApiException
     * @throws \AuroraVision\Exceptions\AuroraVisionException
     */
    private function request($method, $path, array $params = [], $data = null)
    {
        $this->lastResponseRaw = null;
        $this->lastResponse = null;

        if (!empty($params) && !empty(array_filter($params))) {
            $path .= '?' . http_build_query(array_filter($params));
        }

        $curl = curl_init($this->url . $path);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['X-AuroraVision-Token: '. $this->token]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->curlConnectTimeout);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->curlTimeout);
        curl_setopt($curl, CURLOPT_USERAGENT, self::USER_AGENT);

        curl_setopt($curl, CURLINFO_HEADER_OUT, true);

        if ($data !== null) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $this->lastResponseRaw = curl_exec($curl);

        $errorNumber = curl_errno($curl);
        $error = curl_error($curl);
        curl_close($curl);

        if ($errorNumber) {
            throw new AuroraVisionException('CURL: ' . $error, $errorNumber);
        }

        $this->lastResponse = $response = json_decode($this->lastResponseRaw, true);

        if (empty($response)) {
            $e = new AuroraVisionException('Invalid API response');
            $e->rawResponse = $this->lastResponseRaw;
            throw $e;
        }

        return $response;
    }

    /**
     * Performs an authentication request to the AuroraVision API, and stores the given Token.
     *
     * @return string The session token.
     * @throws \AuroraVision\Exceptions\ApiException
     * @throws \AuroraVision\Exceptions\AuroraVisionException
     */
    public function authenticate(): string
    {
        $lastResponseRaw = null;
        $lastResponse = null;

        $curl = curl_init($this->url . '/api/rest/authenticate');
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['X-AuroraVision-ApiKey: '. $this->apiKey]);
        curl_setopt($curl, CURLOPT_USERPWD, $this->username . ":" . $this->password);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->curlConnectTimeout);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->curlTimeout);
        curl_setopt($curl, CURLOPT_USERAGENT, self::USER_AGENT);

        $lastResponseRaw = curl_exec($curl);

        $errorNumber = curl_errno($curl);
        $error = curl_error($curl);
        curl_close($curl);

        if ($errorNumber) {
            throw new AuroraVisionException('Error while authenticating: ' . $error, $errorNumber);
        }

        $response = json_decode($lastResponseRaw, true);

        if (!isset($response['result'])) {
            $e = new AuroraVisionException('Invalid API response while authenticating...');
            $e->rawResponse = $lastResponseRaw;
            throw $e;
        }

        $this->token = $response['result'];
        return $response['result'];
    }

    /**
     * Retrieves the regions/states for a country.
     *
     * @param string $countryCode An ISO 3166-1 alpha-2 compliant country code
     * @return array
     */
    public function region(string $countryCode): array
    {
        $path = '/api/rest/v1/region';
        return $this->get($path, ['countryCode' => $countryCode]);
    }


    /**
     * Retrieves info on a specific asset.
     *
     * @param integer $id Asset entity ID
     * @return array
     */
    public function asssetInfo(int $id): array
    {
        $path = "/api/rest/v1/asset/$id/info";
        return $this->get($path);
    }

    /**
     * Retrieves the list of plants in a portfolio.
     *
     * @param integer $id Portfolio entity ID
     * @param integer $page Page index
     * @return array
     */
    public function portfolioPlants(int $id, int $page = null): array
    {
        $path = "/api/rest/v1/portfolio/$id/plants";
        return $this->get($path, ['page' => $page]);
    }

    /**
     * Retrieves plant groups in a portfolio.
     *
     * @param integer $id Portfolio entity ID
     * @return array
     */
    public function portfolioPlantGroups(int $id): array
    {
        $path = "/api/rest/v1/portfolio/$id/plantGroups";
        return $this->get($path);
    }

    /**
     * Retrieves info on a specific portfolio.
     *
     * @param integer $id Portfolio entity ID
     * @return array
     */
    public function portfolioInfo(int $id): array
    {
        $path = "/api/rest/v1/portfolio/$id/info";
        return $this->get($path);
    }

    /**
     * Retrieves the list of loggers in a specific plant.
     *
     * @param integer $id Plant entity ID
     * @return array
     */
    public function plantLoggers(int $id): array
    {
        $path = "/api/rest/v1/plant/$id/loggers";
        return $this->get($path);
    }

    /**
     * Retrieves plant's lifetime and current day produced energy.
     *
     * @param integer $id Plant entity ID
     * @return array
     */
    public function plantBillingData(int $id): array
    {
        /* TODO: There's also a request body parameter called "locale",
        but it has little to none documentation about it's format. */
        $path = "/api/rest/v1/plant/$id/billingData";
        return $this->get($path);
    }

    /**
     * Retrieves daily produced energy.
     *
     * @param integer $id Plant entity ID
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @return array
     */
    public function plantDailyProduction(int $id, string $startDate, string $endDate): array
    {
        /* TODO: There's also a request body parameter called "locale",
        but it has little to none documentation about it's format. */
        $path = "/api/rest/v1/plant/$id/dailyProduction";
        return $this->get($path, ['startDate' => $startDate, 'endDate' => $endDate]);
    }

    /**
     * Retrieves info on a specific plant.
     *
     * @param integer $id Plant entity ID
     * @return array
     */
    public function plantInfo(int $id): array
    {
        $path = "/api/rest/v1/plant/$id/info";
        return $this->get($path);
    }

    /**
     * Retrieves the status on a plant and its monitored loggers.
     *
     * @param integer $id Plant entity ID
     * @return array
     */
    public function plantStatus(int $id): array
    {
        $path = "/api/rest/v1/plant/$id/status";
        return $this->get($path);
    }

    /**
     * Retrieves plant events.
     *
     * @param integer $id Plant entity ID
     * @param integer $page Page index
     * @param string $eventsType Events type
     * @param string $eventsState Events state
     * @param string $eventsOccurrence
     * @param string $eventsKind
     * @return array
     */
    public function plantEvents(int $id, string $eventsKind, int $page = null, string $eventsType = null, string $eventsState = null, string $eventsOccurrence = null): array
    {
        $path = "/api/rest/v1/plant/$id/events";
        return $this->get($path, [
            'page' => $page,
            'eventsType' => $eventsType,
            'eventsState' => $eventsState,
            'eventsOccurrence' => $eventsOccurrence,
            'eventsKind' => $eventsKind,
        ]);
    }

    /**
     * Retrieves the list of devices monitored by a specific logger.
     *
     * @param integer $id Logger entity ID
     * @return array
     */
    public function loggerDevices(int $id): array
    {
        $path = "/api/rest/v1/logger/$id/devices";
        return $this->get($path);
    }

    /**
     * Retrieves info on a specific logger.
     *
     * @param integer $id Logger entity ID
     * @return array
     */
    public function loggerInfo(int $id): array
    {
        $path = "/api/rest/v1/logger/$id/info";
        return $this->get($path);
    }

    /**
     * Retrieves the status on a logger and it's monitored devices.
     * Use the loggerEvents function/API call for a list of active and/or historical events ocurred on the logger and on its monitored devices.
     *
     * @param integer $id Logger entity ID
     * @return array
     */
    public function loggerStatus(int $id): array
    {
        $path = "/api/rest/v1/logger/$id/status";
        return $this->get($path);
    }

    /**
     * Retrieves logger events.
     *
     * @param integer $id Logger entity ID
     * @param integer $page Page index
     * @param string $eventsType Events type
     * @param string $eventsState Events state
     * @param string $eventsOccurrence
     * @param string $eventsKind
     * @return array
     */
    public function loggerEvents(int $id, string $eventsKind, int $page = null, string $eventsType = null, string $eventsState = null, string $eventsOccurrence = null): array
    {
        $path = "/api/rest/v1/logger/$id/events";
        return $this->get($path, [
            'page' => $page,
            'eventsType' => $eventsType,
            'eventsState' => $eventsState,
            'eventsOccurrence' => $eventsOccurrence,
            'eventsKind' => $eventsKind,
        ]);
    }

    /**
     * Retrieves info on a specific device.
     *
     * @param integer $id Device entity ID
     * @return array
     */
    public function deviceInfo(int $id): array
    {
        $path = "/api/rest/v1/device/$id/info";
        return $this->get($path);
    }

    /**
     * Retrieves the status on a device.
     * Use the deviceEvents function/API call for a list of active and/or historical events ocurred on the device.
     *
     * @param integer $id Device entity ID
     * @return array
     */
    public function deviceStatus(int $id): array
    {
        $path = "/api/rest/v1/device/$id/status";
        return $this->get($path);
    }

    /**
     * Retrieves device events.
     *
     * @param integer $id Device entity ID
     * @param integer $page Page index
     * @param string $eventsType Events type
     * @param string $eventsState Events state
     * @param string $eventsOccurrence
     * @param string $eventsKind
     * @return array
     */
    public function deviceEvents(int $id, string $eventsKind, int $page = null, string $eventsType = null, string $eventsState = null, string $eventsOccurrence = null): array
    {
        $path = "/api/rest/v1/device/$id/events";
        return $this->get($path, [
            'page' => $page,
            'eventsType' => $eventsType,
            'eventsState' => $eventsState,
            'eventsOccurrence' => $eventsOccurrence,
            'eventsKind' => $eventsKind,
        ]);
    }

    /**
     * Retrieves aggregated power data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function powerAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/power/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries power data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function powerTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/power/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves aggregated energy data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function energyAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/energy/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries energy data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function energyTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/energy/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves aggregated performanceratio data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function performanceratioAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/performanceratio/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries performanceratio data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function performanceratioTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/performanceratio/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves aggregated voltage data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function voltageAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/voltage/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries voltage data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function voltageTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/voltage/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves aggregated current data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function currentAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/current/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries current data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function currentTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/current/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves aggregated frequency data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function frequencyAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/frequency/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries frequency data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function frequencyTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/frequency/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves aggregated temperature data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function temperatureAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/temperature/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries temperature data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function temperatureTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/temperature/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves aggregated wind data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $timeZone
     * @return array
     */
    public function windAggregated(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/wind/aggregated/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'timeZone' => $timeZone,
        ]);
    }

    /**
     * Retrieves timeseries wind data.
     *
     * @param integer $id Entity ID
     * @param string $dataType Data type
     * @param string $valueType Value type
     * @param string $startDate Start date (pattern yyyyMMdd)
     * @param string $endDate End date (pattern yyyyMMdd)
     * @param string $sampleSize Sample size
     * @param string $timeZone
     * @return array
     */
    public function windTimeseries(int $id, string $dataType, string $valueType, string $startDate, string $endDate, string $sampleSize, string $timeZone): array
    {
        $path = "/api/rest/v1/stats/wind/timeseries/$id/$dataType/$valueType";
        return $this->get($path, [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sampleSize' => $sampleSize,
            'timeZone' => $timeZone,
        ]);
    }
}
