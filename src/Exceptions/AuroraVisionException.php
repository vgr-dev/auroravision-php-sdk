<?php
namespace vgr\AuroraVision\Exceptions;

use Exception;

/**
 * Generic Exception
 */
class AuroraVisionException extends Exception
{
    /**
     * Last response from API that triggered this exception
     *
     * @var string
     */
    public $rawResponse;
}