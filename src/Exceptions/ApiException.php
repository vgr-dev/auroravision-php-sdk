<?php
namespace vgr\AuroraVision\Exceptions;

/**
 * Printful exception returned from the API
 */
class ApiException extends AuroraVisionException
{
}