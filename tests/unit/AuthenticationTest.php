<?php

use vgr\AuroraVision\ApiClient;

class AuthenticationTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testAuthentication()
    {
        $client = new ApiClient(getenv('AURORAVISION_API_KEY'), getenv('AURORAVISION_USERNAME'), getenv('AURORAVISION_PASSWORD'));
        $this->assertInternalType('string', $client->authenticate());
    }
}